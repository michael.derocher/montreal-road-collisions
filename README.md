# 520-Project-Derocher-Bourne

Express + React app

Description: This app displays a heatmap of the Montreal area that shows the areas that have the most road collisions. It will also provide ways for the user to interact with the map and choose what data to display.

## Attributions

This app uses a dataset provided by 'Service de l'urbanisme et de la mobilité - Direction de la mobilité' for the details on road collisions in Montreal. 

This app also uses the plotly library in React to display the heatmap to the user.

## Structure

There are two directories in the __root__ of the project.

* The Express server is in `server/`
* The React app is in `client/`
* The server responds to API calls and serves the __built__ React app.

There are 3 package.json files -- see what `scripts` they define.

## Setup

To install all the dependencies and build the React app run:

```
npm run build
```

## To run the app

### Just the client

If `client/package.json` has a `proxy` line, remove it. 

```
cd client
npm start
```

### Just the server

```
cd server
node api.mjs
```

### Client and Server

```
cd server
node api.mjs
cd ../client
npm start
```
