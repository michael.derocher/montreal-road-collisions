import './App.css';
import Stub from './components/Stub.js';

function App() {
  return (
    <div className="App">
      <Stub />
    </div>
  );
}

export default App;
