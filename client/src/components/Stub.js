import { useEffect, useState } from 'react';

export default function DisplayStub() {
  const [data, setData] = useState([]);
  async function fetchData(){
    const response = await fetch('/api/10/01');
    const fetchedData = await response.json();
    setData(fetchedData);
  }
  useEffect(() => {
    fetchData();
  }, []);
  // (async () => {
  //   try {
  //     const dataRows = data.map((d) => {
  //       return <DateReport
  //         key={d._id}
  //         date={d.date}
  //         weekday={d.weekday}
  //         accidentType={d.accident_type}
  //         coordinates={d.coordinates}
  //         stats={d.stats}
  //          />;
  //     });
  //   }
  //   catch (e) {
  //     Console.error(e);
  //   }
  // });
  return(
    <div>
      <h1>test print data</h1>
      <ul>
        {data.map((d) => {
          return <DateReport
            key={d._id}
            date={`${d.date.year}-${d.date.month}-${d.date.day}`}
            weekday={d.weekday}
            accidentType={d.accident_type}
            coordinates={`[${d.coordinates.coordinates[0]}, ${d.coordinates.coordinates[1]}]`}
            stats={`Trucks: ${d.stats.trucks},` +
            `Bikes: ${d.stats.bicycles}, Pedestrians: ${d.stats.pedestrians}`}
          />;
        })}
      </ul>
    </div>
  );
}

function DateReport({ date, weekday, acciedentType, coordinates, stats }) {
  return (
    <li>{date} {weekday} {acciedentType} {coordinates} {stats}</li>
  );
}

// function Article({articleTitle, views, rank}) {
//   const [articleData, setArticleData] = useState(null);

//   function renderImage() {
//     if (articleData.originalimage === undefined) {
//       return (
//       <img src="https://st4.depositphotos.com/14953852/22772/v/450/depositphotos_227725020-stock-
//         illustration-image-available-icon-flat-vector.jpg"
//         className="article-img"
//         alt="not found">
//         </img>
//       );
//     } else {
//       return (
//         <img className="article-img"
//           src={articleData.originalimage.source} alt={articleData.title}></img>
//       );
//     }
//   }
