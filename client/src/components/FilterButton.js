 export default function FilterBtn({ filterMethod, buttonName }) {
  return (
    <button onClick={filterMethod}>
      {buttonName}
    </button>
  );
}