import { Icon  } from 'leaflet';
import { useState } from 'react';
import { 
  MapContainer, 
  TileLayer, 
  Marker,
  Popup
} from 'react-leaflet';

import 'leaflet/dist/leaflet.css';
import './Map.css';
// import markerImage from '../img/marker-icon.png';

export default function CollisionMap(){
  const [points, setPoints] = useState();
  const [sliderFilter, setSliderFilter] = useState();

  const attribution = 
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
  const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  
  return (
    <div className="map-container">
      <MapContainer
        center={[45.5, -73.6]}
        zoom={12}
        zoomControl={true}
        updateWhenZooming={false}
        updateWhenIdle={true}
        preferCanvas={true}
        minZoom={10}
        maxZoom={16}
      >
        <TileLayer
          attribution={attribution}
          url={tileUrl}
        />    
        <Marker position={points} icon={customIcon} >
          <Popup><p>A point</p></Popup>
        </Marker>
      </MapContainer>
    </div>
  );
}