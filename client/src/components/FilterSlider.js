export default function FilterSliderDisp({ setSliderFilter }){
  return (
    <form>
      <FilterInput value="all" setSliderFilter={setSliderFilter} />
      <FilterInput value="month" setSliderFilter={setSliderFilter} />
      <FilterInput value="date" setSliderFilter={setSliderFilter} />
    </form>
  );
}

function FilterInput({ value, setSliderFilter }){
  const onFilterChange = e => {
    setSliderFilter(e.target.value);
  };
  return (
    <>
      <input className="slider-filter-option"
        type="radio"
        value={value}
        name="slider-filter-options"
        onChange={onFilterChange} />
      <label htmlFor={value}>{value}</label>
    </>
  );
}