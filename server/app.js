const DB = require('./db/db.js');
const express = require('express');
const compression = require('compression');

const app = express();
const db = new DB();
db.connect('Cluster0', 'montrealRoadCollisions');

app.use(compression());

// Add Cache-control to all responses
app.use(function (req, res, next) {
  res.set('Cache-control', 'public, max-age=31536000');
  next();
});

/**
 * This endpoint gets all the road collisions for a given day of a given month.
 */
app.get('/api/:month/:day', async (req, res) => {
  const month = req.params.month;
  const day = req.params.day;
  if(validateMonth(month)){
    try{
      const dateData = await db.getDay(month, day);
      res.json(dateData);
    } catch {
      res.status(500).send('An error occured while getting the data.');
    }
  } else {
    res.status(401).send('Invalid month parameter.');
  }
});

/**
 * This enpoint gets all the road collisions for a given month.
 */
app.get('/api/:month', async (req, res) => {
  const month = req.params.month;
  if(validateMonth(month)){
    const dateData = await db.getMonth(month);
    res.json(dateData);
  }
});

/**
 * This enpoint gets all the road collisions in the database.
 */
app.get('/api', async (req, res) => {
  const allData = await db.getAll();
  res.json(allData);
});

app.use(express.static('../client/build'));

// app.get('/api/:date', (req, res) => {
//   const date = req.params.date;
//   const dateData = db.getDate(date);
//   res.send(dateData);
// });

// app.get('/api/:vehicle', (req, res) => {
//   switch(req.params.vehicle) {
//     case('car'):
//       return 31;
//     case('')
//   }
//   const vehicle = req.params.vehicle;
//   const vehicleData = getVehicle(vehicle);
//   res.send(vehicleData);
// });

function validateMonth(month){
  const validMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  return validMonths.includes(month);
}

module.exports = app;