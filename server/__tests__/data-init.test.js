const parseRoadCollisions = require('../utils/data-init.js');
const fs = require('node:fs/promises');
const {expect, test, describe } = require('@jest/globals');

// https://www.sammeechward.com/mocking-a-database-with-jest-in-javascript
// https://dawsoncollege.gitlab.io/520JS/520-Web/lectures/07_3_Express_unit_tests.html#mocking
// https://dawsoncollege.gitlab.io/520JS/520-Web/lectures/07_2_unit_testing.html

jest.mock('fs');

describe('parseRoadCollisions', () => {
  beforeEach(() => {
    fs.readFile = jest.fn().mockResolvedValue(
      // eslint-disable-next-line max-len
      ' { "features": [ { "type": "Feature", "properties": { "NO_SEQ_COLL": "SPVM _ 2020 _ 1", "JR_SEMN_ACCDN": "LU", "DT_ACCDN": "2020-09-14", "CD_MUNCP": 66102, "NO_CIVIQ_ACCDN": null, "SFX_NO_CIVIQ_ACCDN": null, "BORNE_KM_ACCDN": null, "RUE_ACCDN": "VOIE DE SERV TRANSCANADIENNE", "TP_REPRR_ACCDN": 2, "ACCDN_PRES_DE": "BD ST CHARLES", "NB_METRE_DIST_ACCD": 300, "CD_GENRE_ACCDN": 45, "CD_SIT_PRTCE_ACCDN": null, "CD_ETAT_SURFC": 11, "CD_ECLRM": 3, "CD_ENVRN_ACCDN": 3, "NO_ROUTE": null, "CD_CATEG_ROUTE": 11, "CD_ETAT_CHASS": null, "CD_ASPCT_ROUTE": 11, "CD_LOCLN_ACCDN": 33, "CD_POSI_ACCDN": null, "CD_CONFG_ROUTE": 1, "CD_ZON_TRAVX_ROUTR": 1, "CD_PNT_CDRNL_ROUTE": null, "CD_PNT_CDRNL_REPRR": null, "CD_COND_METEO": 11, "NB_VEH_IMPLIQUES_ACCDN": 1, "NB_MORTS": 0, "NB_BLESSES_GRAVES": 0, "NB_BLESSES_LEGERS": 0, "HEURE_ACCDN": "22:00:00-22:59:00", "AN": 2020, "NB_VICTIMES_TOTAL": 0, "GRAVITE": "Dommages matériels seulement", "REG_ADM": "Montréal (06)", "MRC": "Montréal (66 )", "nb_automobile_camion_leger": 1, "nb_camionLourd_tractRoutier": 0, "nb_outil_equipement": 0, "nb_tous_autobus_minibus": 0, "nb_bicyclette": 0, "nb_cyclomoteur": 0, "nb_motocyclette": 0, "nb_taxi": 0, "nb_urgence": 0, "nb_motoneige": 0, "nb_VHR": 0, "nb_autres_types": 0, "nb_veh_non_precise": 0, "NB_DECES_PIETON": 0, "NB_BLESSES_PIETON": 0, "NB_VICTIMES_PIETON": 0, "NB_DECES_MOTO": 0, "NB_BLESSES_MOTO": 0, "NB_VICTIMES_MOTO": 0, "NB_DECES_VELO": 0, "NB_BLESSES_VELO": 0, "NB_VICTIMES_VELO": 0, "VITESSE_AUTOR": 50, "LOC_X": 276982.781, "LOC_Y": 5034955.0, "LOC_COTE_QD": "A", "LOC_COTE_PD": 2, "LOC_DETACHEE": "N", "LOC_IMPRECISION": "O", "LOC_LONG": -73.855656, "LOC_LAT": 45.453971 }, "geometry": { "type": "Point", "coordinates": [ -73.855655730051652, 45.453971118268093 ] } } ] } '
    );
  });

  test('should respond with json object of parsed data', async () => {
    // check mocking axios module => lecture 07.3
    const response = await parseRoadCollisions();
    expect(response).toEqual(
      [{
        _id: undefined,
        date: {
          year: '2020',
          month: '09',
          day: '14'
        },
        weekday: 'LU',
        // eslint-disable-next-line camelcase
        accident_type: 45,
        coordinates: {
          type: 'Point',
          coordinates: [-73.855655730051652, 45.453971118268093]
        },
        stats: {
          bicycles: 0,
          pedestrians: 0,
          trucks: 1 + 0
        }
      }]
    );
    // expect(response.statusCode).toBe(200);
    // expect(response.type).toEqual('application/json');
  });
});

// describe('parseDate', () => {
//   test('should respond with json object of parsed date', async () => {
//     const date = '2020-10-01';
//     const response = await parseDate(date);
//     expect(response).toEqual(
//       {
//         year: '2020',
//         month: '10',
//         day: '01'
//       }
//     );
//   });
// });