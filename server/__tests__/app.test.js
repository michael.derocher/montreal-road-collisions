const app = require('../app');
const DB = require('../db/db.js');
const request = require('supertest');
const {expect, test, describe} = require('@jest/globals');

jest.mock('../db/db.js');

describe('GET /api', () => {
  test('should respond with json object', async () => {
    jest.spyOn(DB.prototype, 'getAll').mockResolvedValue(
      [{
        id: 0,
        weekday: 'LU',
        accidentType: 32,
        coordinates: [45.5, -73.5],
        stats: {
          bicycles: 0,
          pedestrians: 1,
          trucks: 1 + 0
        }
      }]
    );
    const response = await request(app).get('/api');
    expect(response.body).toEqual(
      [{
        id: 0,
        weekday: 'LU',
        accidentType: 32,
        coordinates: [45.5, -73.5],
        stats: {
          bicycles: 0,
          pedestrians: 1,
          trucks: 1 + 0
        }
      }]
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });
});

// describe('GET /api/date', () => {
//   test('should respond with json object', async () => {
//     jest.spyOn(DB.prototype, 'readAll').mockResolvedValue(
//       [{
//         id: 0,
//         weekday: 'LU',
//         accidentType: 32,
//         coordinates: [45.5, -73.5],
//         stats: {
//           bicycles: 0,
//           pedestrians: 1,
//           trucks: 1 + 0
//         }
//       }]
//     );
//     const response = await request(app).get('/api/');
//     expect(response.body).toEqual(
//       [{
//         id: 0,
//         weekday: 'LU',
//         accidentType: 32,
//         coordinates: [45.5, -73.5],
//         stats: {
//           bicycles: 0,
//           pedestrians: 1,
//           trucks: 1 + 0
//         }
//       }]
//     );
//     expect(response.statusCode).toBe(200);
//     expect(response.type).toEqual('application/json');
//   });
// });