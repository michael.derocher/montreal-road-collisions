/* eslint-disable no-unused-vars */
const DB = require('../db/db.js');
const {expect, test, describe} = require('@jest/globals');
const request = require('supertest');
const app = require('../app.js');

// https://www.sammeechward.com/mocking-a-database-with-jest-in-javascript
// https://dawsoncollege.gitlab.io/520JS/520-Web/lectures/07_3_Express_unit_tests.html#mocking
// https://dawsoncollege.gitlab.io/520JS/520-Web/lectures/07_2_unit_testing.html

jest.mock('../db/db.js');

describe('getAll', () => {
  test('should respond with json object', async () => {
    jest.spyOn(DB.prototype, 'getAll').mockResolvedValue(
      [{
        id: 0,
        weekday: 'LU',
        accidentType: 32,
        coordinates: [45.5, -73.5],
        stats: {
          bicycles: 0,
          pedestrians: 1,
          trucks: 1 + 0
        }
      }]
    );
    const response = await request(app).get('/api');
    expect(response.body).toEqual(
      [{
        id: 0,
        weekday: 'LU',
        accidentType: 32,
        coordinates: [45.5, -73.5],
        stats: {
          bicycles: 0,
          pedestrians: 1,
          trucks: 1 + 0
        }
      }]
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });
});