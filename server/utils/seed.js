const parseRoadCollisions = require('./data-init.js');
const DB = require('../db/db.js');

/**
 * IFFE function that gets the parsed data from data-init.mjs, connects to the mongoDB and inserts
 * all the data into the collection. Also indexes the collection on the month and day.
 */
(async () => {
  let db;
  try {
    const allCollisions = await parseRoadCollisions();
    db = new DB();
    // dbname is cluster0 in my case
    await db.connect('cluster0', 'montrealRoadCollisions');
    const num = await db.createMany(allCollisions);
    // eslint-disable-next-line no-console
    console.log(`Inserted ${num} collisions`);
    await db.index( { 'date.month': 1, 'date.day': 1 } );
  } catch (e) {
    console.error('could not seed');
    // eslint-disable-next-line no-console
    console.dir(e);
  } finally {
    if (db) {
      db.close();
    }
    process.exit();
  }
})();
