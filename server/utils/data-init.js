const fs = require('node:fs/promises');

/**
 * Parses through every entry from the 2020_collisions_routieres.geojson file and
 * creates road collision objects for each collision.
 * @returns An array of road collision objects
 */
async function parseRoadCollisions(){
  const fetchData = await fs.readFile('../data/2020_collisions_routieres.geojson', 'utf-8');
  const roadCollisions = await JSON.parse(fetchData);
  const parsedCollisions = [];
  for(const collision of roadCollisions.features){
    const parsedCollision = {
      '_id': undefined,
      'date': parseDate(collision.properties.DT_ACCDN),
      'weekday': collision.properties.JR_SEMN_ACCDN,
      'accident_type': collision.properties.CD_GENRE_ACCDN,
      'coordinates': collision.geometry,
      'stats': {
        'bicycles': collision.properties.nb_bicyclette,
        'pedestrians': collision.properties.NB_VICTIMES_PIETON,
        'trucks': collision.properties.nb_camionLourd_tractRoutier +
         collision.properties.nb_automobile_camion_leger
      }
    };
    parsedCollisions.push(parsedCollision);
  }
  return parsedCollisions;
}

/**
 * Parses a date string into an object
 * @param {String} date 
 * @returns An object with properties for the year, month, and day
 */
function parseDate(date){
  const splitDate = date.split('-');
  return {
    'year': splitDate[0],
    'month': splitDate[1],
    'day': splitDate[2]
  };
}

module.exports = parseRoadCollisions;