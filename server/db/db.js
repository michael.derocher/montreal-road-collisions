const dotenv = require('dotenv');
dotenv.config();
const dbUrl = process.env.ATLAS_URI;
const { MongoClient } = require('mongodb');

let instance = null;

class DB {

  constructor(){
    //instance is the singleton, defined in outer scope
    if (!instance){
      instance = this;
      this.client = new MongoClient(dbUrl);
      this.db = null;
      this.collection = null;
    }
    return instance;
  }

  async connect(dbname, collName) {
    if (instance.db){
      return;
    }
    await instance.client.connect();
    instance.db = await instance.client.db(dbname);
    // Send a ping to confirm a successful connection
    await instance.client.db(dbname).command({ ping: 1 });
    instance.collection = await instance.db.collection(collName);
  }

  async close() {
    await instance.client.close();
    instance = null;
  }

  async getAll() {
    return await instance.collection.find().toArray();
  }

  async getMonth(month){
    return await instance.collection.find({'date.month': month}).toArray();
  }

  async getDay(month, day){
    return await instance.collection.find({'date.month': month, 'date.day': day}).toArray();
  }

  async getWeekday(month, weekday){
    return await instance.collection.find({'date.month': month, 'weekday': weekday}).toArray();
  }

  async create(collision){
    return await instance.collection.insertOne(collision);
  }

  async createMany(collisions){
    const promises = await Promise.all(collisions.map(this.create));
    return promises.length;
  }

  async open(dbname, collName) {
    try {
      await instance.connect(dbname, collName);
    } finally {
      await instance.close();
    }
  }

  async index(column){
    return instance.collection.createIndex({ [column] : 1 });
  }

  async compoundIndex(fields){
    return instance.collection.createIndex(fields);
  }

}

module.exports = DB;
