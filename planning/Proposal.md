## Data
[https://gitlab.com/csy3dawson23-24/520/teams/TeamB12-KevinMichael/520-project-derocher-bourne/-/issues/16](https://gitlab.com/csy3dawson23-24/520/teams/TeamB12-KevinMichael/520-project-derocher-bourne/-/issues/16)

## API
- There will be an endpoint for getting all the road collisions from a specified month of the year.
- There will also be an endpoint for getting all the road collisions from a specified day of the week (i.e. mon, tue, wed, etc...)

## Visualizations
Our webapp will display information regarding road collision in Montreal in the year 2020. The graphic representation we decided to use is a group bar graph that will represent total incidents, and then incidents involving pedestrians, bicycles, and trucks specifically by month. It will be able to be filtered by the specific type of collision (as mentioned previously) as well as by weekday. 

## Views
- display wireframe(s), explain whats above and what below the fold
- wireframe includes map configuration that may not be implemented after all

## Functionality
- There will be buttons to apply filters for type of collision
- There will be a slider for selecting the day of the week

## Features and Priorities
Core features:
- Group bargraph representing number of collision by month in a year (2020)
- Slider for selecting the weekday
- Filters for selecting specific collision event type

Potential features:
- Extra filters for things like severity of collision or time of day

## Dependencies
- We will use the plotly React library to display the group bar chart on the client-side.
-- what libraries did you consider and how did you decide what to use
