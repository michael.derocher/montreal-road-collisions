## ROLES
Coordinator :	Michael + Kevin

Monitor :	Michael

Checker	 :	Kevin

## WEEKLY SCHEDULING
Meeting / Worktime : Monday 2:30-4pm Thursday 1-2pm (during lab time)

## POLICIES
Response time :
-	Weekday 4-6 hours
-	Saturday 6 hours
-	Sunday 12 hours

In case of emergency :
-	Communicate to teammate as soon as possible
-	Communicate to teammate how emergency will/might impact work
-	Adjust goals/scope of project according to emergency impact and redistribute as fitting

Communication : Discord, phone/text, in person

General Agreements:
-	Complete expected goals by agreed upon date/time
-	Be punctual and prepared for meetings
-	Ensure that questions don't go unanswered for longer than expected (outlined in response time)
-	Ensure that larger tasks are distributed so that we both get to work on parts of it
